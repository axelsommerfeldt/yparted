/*
	ostream.h

	(Poor) Implementation of output streams which offer prefixes and
	enabling/disabling.

	Copyright (C) 2021 Axel Sommerfeldt (axel.sommerfeldt@fastmail.de)

	--------------------------------------------------------------------------

	This file is part of yparted: https://gitlab.com/axelsommerfeldt/yparted

	yparted is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option) any later version.

	yparted is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yparted.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DULL_OSTREAM_H
#define DULL_OSTREAM_H

#include <iostream>
#include <utility>

namespace dull
{

template <class substream> class ostream;

template <class substream>
class ostream
{
	substream &dest;     // destination stream
	const char *prefix;  // prefix added to beginning of each line
	bool bol;            // begin-of-line flag
	bool on;             // is this thing on?

public:
	ostream( substream &dest, const char *prefix, bool enabled = true )
		: dest( dest ), prefix( prefix ), bol( true ), on( enabled )
	{}

	void setbol() { bol = true; }

	bool enabled() const { return on; }
	void enable( bool on ) { ostream::on = on; }

	// Forward all output operations to the destination stream
	template <typename T>
	ostream &operator<<( T &&obj )
	{
		if ( on )
		{
			if ( bol ) { bol = false; dest << prefix; }
			dest << std::forward<T>( obj );
		}

		return *this;
	}

	// Special handling of std::endl
	// Note that "mod_func" could be anything (std::endl, std::ends, ...)
	// but for the sake of simplicity we assume it's std::endl ;-)
	ostream &operator<<( substream &(*mod_func)( substream & ) )
	{
		if ( on ) { (*mod_func)( dest ); setbol(); }
		return *this;
	}

	// Handle own I/O manipulators
	ostream &operator<<( ostream &(*mod_func)( ostream & ) )
	{
		if ( on ) (*mod_func)( *this );
		return *this;
	}
};

// (Output-only) I/O manipulator to mark the begin of a new line
template <class substream>
inline ostream<substream>& beginl( ostream<substream>& os )
{
	os.setbol();
	return os;
}

// (Output-only) I/O manipulator to insert a newline character into the output sequence os
template <class substream>
inline ostream<substream>& endl( ostream<substream>& os )
{
//	return os << std::endl << beginl;
	return os << std::endl;
}

// (Output-only) I/O manipulator to enable/disable output stream
struct _setenabled { bool on; };
inline _setenabled setenabled( bool on ) { return { on }; }
template <class substream>
inline ostream<substream> &operator<<( ostream<substream> &os, dull::_setenabled obj )
{
	os.enable( obj.on );
	return os;
}

}

#endif

