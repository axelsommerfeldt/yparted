/*
	logging.cpp

	Implementation of additional output streams based on std::cout and std::cerr.

	Copyright (C) 2021 Axel Sommerfeldt (axel.sommerfeldt@fastmail.de)

	--------------------------------------------------------------------------

	This file is part of yparted: https://gitlab.com/axelsommerfeldt/yparted

	yparted is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option) any later version.

	yparted is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yparted.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "logging.h"

#include <iostream>

dull::ostream<std::ostream> cerr   { std::cerr, "[ERROR] ",   true  };
dull::ostream<std::ostream> cwarn  { std::cerr, "[WARNING] ", true  };
dull::ostream<std::ostream> cout   { std::cout, "",           true  };
dull::ostream<std::ostream> cinfo  { std::cout, "[INFO] ",    false };
dull::ostream<std::ostream> cdebug { std::cout, "[DEBUG] ",   false };

bool increase_log_level()
{
	if ( !cinfo.enabled() )
		cinfo << dull::setenabled( true );
	else if ( !cdebug.enabled() )
		cdebug << dull::setenabled( true );
	else
		return false;  // we have already reached max. log level

	return true;
}

