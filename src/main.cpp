/*
	main.cpp

	Main source file of yparted, it contains the option and command
	processing.

	Copyright (C) 2021 Axel Sommerfeldt (axel.sommerfeldt@fastmail.de)

	--------------------------------------------------------------------------

	This file is part of yparted: https://gitlab.com/axelsommerfeldt/yparted

	yparted is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option) any later version.

	yparted is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yparted.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "options.h"
#include "logging.h"

#include <iostream>
#include <cstring>
#include <exception>

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
const char yparted_version[] = "yparted version " TOSTRING(YPARTED_VERSION);

void usage()
{
	std::cerr << "Usage: yparted [OPTIONS] COMMAND DISK [PARTNO] [KEY]..." << std::endl;

	std::cerr << std::endl;

	std::cerr << "List of commands:" << std::endl;
	std::cerr << std::endl;
	// ...

	std::cerr << std::endl;

	std::cerr << "List of keys:" << std::endl;
	std::cerr << std::endl;
	// ...

	std::cerr << std::endl;

	std::cerr << "List of options:" << std::endl;
	std::cerr << std::endl;
	std::cerr << "--verbose               Verbose operation, show debug messages" << std::endl;
	// ...

	std::cerr << std::endl;

	std::cerr << "Example usage:" << std::endl;
	std::cerr << std::endl;
	// ...

	std::cerr << std::endl;
}

int main( int argc, char * const argv[] )
{
	if ( argc < 2 || strcmp( argv[1], "-?" ) == 0 || strcmp( argv[1], "--help" ) == 0 )
	{
		usage();
		return 0;
	}

	if ( strcmp( argv[1], "--version" ) == 0 )
	{
		std::cout << yparted_version << std::endl;
		return 0;
	}

	try
	{
		auto command = get_next_arg( argc, argv );
		if ( command == nullptr )
		{
			usage();
			return 1;
		}

		cinfo << yparted_version << std::endl;

		if ( strcmp( command, "info" ) == 0 )
		{
			;  // TODO
		}
		else if ( strcmp( command, "list" ) == 0 )
		{
			;  // TODO
		}

		else if ( strcmp( command, "get" ) == 0 )
		{
			;  // TODO
		}
		else if ( strcmp( command, "set" ) == 0 )
		{
			;  // TODO
		}

		else if ( strcmp( command, "backup" ) == 0 )
		{
			;  // TODO
		}
		else if ( strcmp( command, "restore" ) == 0 )
		{
			;  // TODO
		}

		else
		{
			cerr << "Unrecognized command \"" << command << "\"" << std::endl;
			return 1;
		}
	}
	catch ( const std::exception &ex )
	{
		cerr << ex.what() << std::endl;
		return 1;
	}

	return 0;
}

