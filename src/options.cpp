/*
	options.h

	Functions used for command option & argument parsing,
	furthermore implementation of variables holding the values of options.

	Copyright (C) 2021 Axel Sommerfeldt (axel.sommerfeldt@fastmail.de)

	--------------------------------------------------------------------------

	This file is part of yparted: https://gitlab.com/axelsommerfeldt/yparted

	yparted is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option) any later version.

	yparted is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yparted.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "options.h"
#include "logging.h"
#include "exception.h"

#include <getopt.h>

const char *get_next_arg( int argc, char * const argv[] )
{
	static option long_options[] =
	{
		{ "verbose",        no_argument,       nullptr, 'v' },
		{ nullptr,          0,                 nullptr,  0 }
	};

	for (;;)
	{
		int long_index = 0;
		auto ch = getopt_long( argc, argv, "-v", long_options, &long_index );
		if ( ch == EOF ) break;

		switch ( ch )
		{
		case 'v':
			increase_log_level();
			break;

		case 1:
			return optarg;

		default:
			// Note: getopt_long() has already output an error message here to stderr.
			throw exception( "Try 'yparted --help' for more information." );
		}
	}

	return ( optind < argc ) ? argv[optind++] : nullptr;
}

