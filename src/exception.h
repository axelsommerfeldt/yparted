/*
	exception.h

	Definition of own exception class, based on std::exception.

	Copyright (C) 2021 Axel Sommerfeldt (axel.sommerfeldt@fastmail.de)

	--------------------------------------------------------------------------

	This file is part of yparted: https://gitlab.com/axelsommerfeldt/yparted

	yparted is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option) any later version.

	yparted is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with yparted.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <exception>
#include <string>

class exception : public std::exception
{
	std::string what_str;

public:
	explicit exception( const std::string &what_arg )
		: what_str( what_arg )
	{}

	explicit exception( std::string &&what_arg )
		: what_str( std::move( what_arg ) )
	{}

	explicit exception( const char* what_arg )
		: what_str( what_arg )
	{}

	const char *what() const noexcept override
	{
		return what_str.c_str();
	}
};

#endif

