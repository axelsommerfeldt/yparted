cmake_minimum_required( VERSION 3.7...3.20 )

project( yparted
	VERSION 0.0.0
#	DESCRIPTION "Low-level partition table inspection & modification tool"
#	HOMEPAGE_URL "https://gitlab.com/axelsommerfeldt/yparted"
	LANGUAGES CXX
	)

if( CMAKE_VERSION VERSION_GREATER_EQUAL "3.9" )
	set( CMAKE_PROJECT_DESCRIPTION "Low-level partition table inspection & modification tool" )
	set( PROJECT_DESCRIPTION "${CMAKE_PROJECT_DESCRIPTION}" )
endif()

if( CMAKE_VERSION VERSION_GREATER_EQUAL "3.12" )
	set( CMAKE_PROJECT_HOMEPAGE_URL "https://gitlab.com/axelsommerfeldt/yparted" )
	set( PROJECT_HOMEPAGE_URL "${CMAKE_PROJECT_HOMEPAGE_URL}" )
endif()

add_executable( yparted src/main.cpp )

target_sources( yparted PRIVATE
	src/options.cpp
	src/logging.cpp
	)

if( CMAKE_VERSION VERSION_GREATER_EQUAL "3.8" )
	target_compile_features( yparted PRIVATE cxx_std_11 )
else()
	set_target_properties( yparted PROPERTIES
		CXX_STANDARD 11
		CXX_STANDARD_REQUIRED YES
		CXX_EXTENSIONS NO
		)
endif()

target_compile_definitions( yparted PRIVATE YPARTED_VERSION=${yparted_VERSION} )
target_compile_options( yparted PRIVATE -pedantic -Wall -Wextra -Wconversion )

# Installing

include( GNUInstallDirs )

install( TARGETS yparted
	RUNTIME
		DESTINATION ${CMAKE_INSTALL_BINDIR}
	)

# Packaging

set( CPACK_PACKAGE_CONTACT "Axel Sommerfeldt <axel.sommerfeldt@fastmail.de>" )
set( PACKAGE_RELEASE 1 )

set( CPACK_DEBIAN_FILE_NAME DEB-DEFAULT )
set( CPACK_DEBIAN_PACKAGE_RELEASE "${PACKAGE_RELEASE}" )
set( CPACK_DEBIAN_PACKAGE_SECTION "admin" )
set( CPACK_DEBIAN_PACKAGE_SHLIBDEPS ON )

set( CPACK_RPM_FILE_NAME RPM-DEFAULT )
set( CPACK_RPM_PACKAGE_RELEASE "${PACKAGE_RELEASE}" )
#set( CPACK_RPM_PACKAGE_GROUP "Unspecified" )
set( CPACK_RPM_PACKAGE_LICENSE "GPLv2" )
set( CPACK_RPM_PACKAGE_AUTOREQ YES )

include( CPack )

# Source code documentation

find_package( Doxygen )
if( Doxygen_FOUND )
	set( DOXYGEN_INPUT_DIR  ${PROJECT_SOURCE_DIR}/src )
	set( DOXYGEN_OUTPUT_DIR ${CMAKE_CURRENT_BINARY_DIR} )
	set( DOXYGEN_INDEX_FILE ${DOXYGEN_OUTPUT_DIR}/html/index.html )

	set( DOXYFILE_IN  ${CMAKE_CURRENT_LIST_DIR}/Doxyfile.in )
	set( DOXYFILE_OUT ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile )
	configure_file( ${DOXYFILE_IN} ${DOXYFILE_OUT} @ONLY )

	file( GLOB_RECURSE YPARTED_SOURCE_FILES ${DOXYGEN_INPUT_DIR}/* )

	add_custom_command(
		OUTPUT ${DOXYGEN_INDEX_FILE}
		COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYFILE_OUT}
		MAIN_DEPENDENCY ${DOXYFILE_OUT} ${DOXYFILE_IN}
		DEPENDS ${YPARTED_SOURCE_FILES}
		WORKING_DIRECTORY ${DOXYGEN_INPUT_DIR}
		COMMENT "Generating source code documentation"
#		VERBATIM
		)
	add_custom_target( doxygen ALL DEPENDS ${DOXYGEN_INDEX_FILE} )
endif()

