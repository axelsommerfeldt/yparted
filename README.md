# yparted

Non-interactive command line tool to inspect or modify existing partition tables on a lower level than fdisk, gdisk, or parted

Tools like fdisk, gdisk, and parted are great for adding and deleting partitions, but their ability to show
or modify all details of the partition table are limited. This tool tries to fill that gap.

yparted is written in C++11.

The project is located at [https://gitlab.com/axelsommerfeldt/yparted](https://gitlab.com/axelsommerfeldt/yparted).

## Prerequisites

...

## Building

yparted is build using CMake version 3.7 or newer, e.g.:

```bash
mkdir build && cd build
cmake -D CPACK_GENERATOR=DEB ..
cmake --build . --target package
```

Installation packages will be build automatically for many Linux distributions by GitLab CI:
[https://gitlab.com/axelsommerfeldt/yparted/-/pipelines](https://gitlab.com/axelsommerfeldt/yparted/-/pipelines)

## Installation

After the package was either build or downloaded successfully, it could be installed with the package management tool
of your Linux distribution, for example apt when running a Debian-based distribution:

```bash
sudo apt install yparted*.deb
```

As an alternative it could be installed right after building, e.g.:

```bash
mkdir build && cd build
cmake ..
cmake --build .
sudo cmake --install .
```

## Usage

Running yparted without command line arguments will give you a short overview of the available commands and options:

```
$ yparted
Usage: yparted [OPTIONS] COMMAND DISK [PARTNO] [KEY]...

List of commands:
...

List of keys:
...

List of options:
...
```

...

## Run tests

...

## Support

Either ask your question in the [Issue Tracker at GitLab](https://gitlab.com/axelsommerfeldt/yparted/-/issues)
or contact me via [email](mailto:axel.sommerfeldt@fastmail.de).

## Roadmap

There is no roadmap yet.

Version 1.0 should include at least support DOS and GPT partition tables and should offer the commands
"info", "list", "get", "set", "backup", and "restore".

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Author

Axel Sommerfeldt

* Email:  [axel.sommerfeldt@fastmail.de](mailto:axel.sommerfeldt@fastmail.de)
* GitLab: [@axelsommerfeldt](https://gitlab.com/axelsommerfeldt)
* GitHub: [@axelsommerfeldt](https://gitlab.com/axelsommerfeldt)

## License

[GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Project status

This project is at a very early stage.
Since I'm developing it in my spare time progress might me slow.

