#!/bin/bash

# codequality-badge
#
# Parses a GitLab CI Code Quality file and creates a corresponding badge
# using "jq" and "anybadge".
#
# Copyright (C) 2021 Axel Sommerfeldt (axel.sommerfeldt@fastmail.de)
#
# --------------------------------------------------------------------------
#
# This file is part of yparted: https://gitlab.com/axelsommerfeldt/yparted
#
# yparted is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# yparted is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with yparted.  If not, see <https://www.gnu.org/licenses/>.

if [[ -z $1 || ! -r $2 ]]
then
	printf 'Usage: codequality-badge LABEL JSONFILE\n' >&2
	exit 1
fi

# Read "severity" entries from JSON input file (using jq)

mapfile -t severities <<< "$(jq --raw-output '.[].severity' "$2")"

# Count severities

info=0
minor=0
major=0
critical=0
blocker=0
unknown=0
total=0

for severity in "${severities[@]}"
do
	if [[ $severity == "info" ]]
	then
		(( ++info ))
	elif [[ $severity == "minor" ]]
	then
		(( ++minor ))
	elif [[ $severity == "major" ]]
	then
		(( ++major ))
	elif [[ $severity == "critical" ]]
	then
		(( ++critical ))
	elif [[ $severity == "blocker" ]]
	then
		(( ++blocker ))
	elif [[ $severity ]]
	then
		printf 'ERROR: Unknown severity "%s"\n' "$severity" >&2
		(( ++unknown ))
	else
		continue
	fi

	(( ++total ))
done

# Choose a badge color dependent on the highest severity

if (( unknown ))
then
	color=black
elif (( blocker ))
then
	color=brightred
elif (( critical ))
then
	color=red
elif (( major ))
then
	color=orange
elif (( minor ))
then
	color=yellow
elif (( info ))
then
	color=yellowgreen
else
	color=green
fi

# Create the badge (using anybadge)

anybadge --label="$1" --value="$total" --overwrite --file="$1.svg" --color="$color"

